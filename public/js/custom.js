$( document ).ready(function() {
  var base_color = "#C4C4C4";
  var active_color = "#7FC3FF";
  var varify_color = "#8CC63F";
  
  
  var child = 1;
  var length = $("section").length - 1;
  $("#submit").addClass("disabled");
  
  $("section").not("section:nth-of-type(1)").hide();
  $("section").not("section:nth-of-type(1)").css('transform','translateX(100px)');
  
  var svgWidth = length * 200 + 24;
  $("#svg_wrap").html(
    '<svg version="1.1" id="svg_form_time" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 ' +
      svgWidth +
      ' 24" xml:space="preserve"></svg>'
  );
  
  function makeSVG(tag, attrs) {
    var el = document.createElementNS("http://www.w3.org/2000/svg", tag);
    for (var k in attrs) el.setAttribute(k, attrs[k]);
    return el;
  }
  
  for (i = 0; i < length; i++) {
    var positionX = 12 + i * 200;
    var rect = makeSVG("rect", { x: positionX, y: 11, width: 200, height: 2 });
    document.getElementById("svg_form_time").appendChild(rect);
    // <g><rect x="12" y="9" width="200" height="6"></rect></g>'
    var circle = makeSVG("circle", {
      cx: positionX,
      cy: 12,
      r: 12,
      width: positionX,
      height: 6
    });
    document.getElementById("svg_form_time").appendChild(circle);
  }
  
  var circle = makeSVG("circle", {
    cx: positionX + 200,
    cy: 12,
    r: 12,
    width: positionX,
    height: 6
  });
  document.getElementById("svg_form_time").appendChild(circle);
  
  $('#svg_form_time rect').css('fill',active_color);
  $('#svg_form_time circle').css('fill',base_color);
  $("circle:nth-of-type(1)").css("fill", active_color);
  
  var clickCount = 0;
  $(".button").click(function () {

    $("#svg_form_time rect").css("fill", active_color);
    $("#svg_form_time circle").css("fill", active_color);
    $("circle:nth-of-type(1)").css("fill", varify_color);

    // if(clickCount == 2){
    //   $("circle:nth-of-type(2)").css("fill", varify_color);
    // }
    
    if (clickCount > 0) {
      $("#secondheading").removeClass("addcss");
      $("#thirdheading").addClass("addcss");
      
    } else {
      $("#firstheading").removeClass("addcss");
      $("#secondheading").addClass("addcss");
      
     
    }
 clickCount++;
  
    
    var id = $(this).attr("id");
    if (id == "next") {
      if (child >= length) {
        $("circle:nth-of-type(2)").css("fill", varify_color);
        $(this).addClass("disabled");
        $('#submit').removeClass("disabled");
      }
      if (child <= length) {
        child++;
      }
    } 
  
    var circle_child = child + 1;
    $("#svg_form_time rect:nth-of-type(n + " + child + ")").css(
      "fill",
      base_color
    );
    $("#svg_form_time circle:nth-of-type(n + " + circle_child + ")").css(
      "fill",
      base_color
    );
    var currentSection = $("section:nth-of-type(" + child + ")");
    currentSection.fadeIn();
    currentSection.css('transform','translateX(0)');
   currentSection.prevAll('section').css('transform','translateX(-100px)');
    currentSection.nextAll('section').css('transform','translateX(100px)');
    $('section').not(currentSection).hide();
  });
  
  });


  $(".clicktoadd").click(function(){
    $(this).parent().css("background-color", "#7FC3FF");
    $(this).parent().css("color", "white");
 });

 $(".fa-check-square").click(function(){
  $(this).parent().css("background-color", "white");
  $(this).parent().css("color", "black");
});


  // $(".clicktoadd").click( function(){
  //   $(".options_box").toggleClass("add_css");
  //   // $(this).toggleClass("fa fa-plus-square");
  //   // $(this).toggleClass("fa fa-check-square");


  //   // $(".options_box").toggleClass("add_css");

  //   // $(this).prev('.options_box').toggleClass("add_css");
  //   // $(this).prev('.options_box').css({
  //   //   "background-color": "#7FC3FF"
  //   // });

  // });


  
  $(".fa-plus-square").click(function() {
    $(this).css({
      "display": "none"
    });
    $(this).next('.fa-check-square').css({
      "background-color": "#7FC3FF",
      "color": "white",
      "display": "inline"
    });
    // $(this).prev().css({"background": "#7FC3FF", "color": "white"});
   
    // document.getElementById("demo").style.background = "#7FC3FF";
    // document.getElementById("demo").style.color = "white";
    // $(this).prev('.options_box').toggleClass('add_css');
  });
  $(".fa-check-square").click(function() {
    $(this).css({
      "display": "none"
    });
    $(this).prev('.fa-plus-square').css({
      "display": "inline"
    });
    // $(this).prev().prev().css({"background": "white", "color": "black"});
    // document.getElementById("demo").style.background = "white";
    // document.getElementById("demo").style.color = "black";
  });