
function App() {
  return (
 <div>
      <div className="container topheader">
      <div className="d-flex align-items-center">
        <img src="img/Icon.png" alt="logo" />
        <h1>Seeker In</h1>
      </div>
      <div className="switchbtn">
        <input type="checkbox" id="toggle_checkbox"/>

        <label for="toggle_checkbox">
          <div id="star">
            <img src="img/light-filled.png" alt="light-filled"/>
          </div>
          <div id="moon"><img src="img/moon.png" alt="moon"/></div>
        </label>
      </div>
    </div>
    <div className="heading">
      <h6 id="firstheading" className="addcss">Create Profile</h6>
      <h6 id="secondheading">Follow Topics</h6>
      <h6 id="thirdheading">Finish Setup</h6>
    </div>
    <div id="svg_wrap"></div>

    <section className="profilesection">
      <div className="coverimgs">
        <div className="backimg">
          <img src="img/coverimg.png" alt="backimg" width="100%"  className="onlaptop"/>
          <img src="img/coveronmobile.png" alt="backimg" width="100%" className="onmobile" />
          <div className="iconimages2">
            <a href="">
              <img src="img/picture.png" alt="profile" className="icon" />
            </a>
            <a href="">
              <img src="img/edit.png" alt="profileedit" className="icon" />
            </a>
          </div>
        </div>

        <div className="profileimg">
          <img src="img/profileimg.png" alt="backimg" width="100%" />

          <div className="iconimages">
            <a href="">
              <img src="img/picture.png" alt="profile" className="icon" />
            </a>
            <a href="">
              <img src="img/edit.png" alt="profileedit" className="icon" />
            </a>
          </div>
        </div>
      </div>
      <div className="text-left">
        <label className="mt-4">Profile Healine</label>
        <input type="text" placeholder="UI/UX Designer.. etc" />
      </div>

      <div className="text-left" style={{display: 'grid'}}>
        <label className="mt-4">Profile Bio</label>
        <textarea rows="5" placeholder="i am..."></textarea>
      </div>

      <div className="row">
        <div className="col-sm-6 text-left">
          <label>Gender</label>
          <div className="gender">
            <input type="radio" name="gender" />
            <label>Male</label>

            <input type="radio" name="gender" />
            <label>Female</label>

            <input type="radio" name="gender" checked />
            <label>Other</label>
          </div>
        </div>
        <div className="col-sm-6 text-left">
          <label>Date of Birth</label>
          <input type="date" placeholder="Birthdate" />
        </div>
      </div>
    </section>

    <section className="Personalizesection">
      <h2 className="text-left">Personalize your Feed</h2>
      <p className="text-left">Follow 5 or more topics for Super Dope Experience</p>

      <select className="form-control">
        <option value="1">#1 Programming</option>
        <option value="2">#1 Programming</option>
        <option value="3">#1 Programming</option>
      </select>

      <div className="select_options">
        <div className="row justify-content-between">
          <div className="mobileshow">
<div className="col-lg-3 col-md-3 col-sm-6">
            <div className="options_box">
              <h6 className="m-0 mr-3">JavaScript</h6>
              <i className="fa fa-plus-square clicktoadd"></i>
              <i className="fa fa-check-square clicktoadd" style={{display:'none'}}></i>
            </div>
          </div>
        <div className="col-lg-3 col-md-3 col-sm-6">
            <div className="options_box">
              <h6 className="m-0 mr-3">JavaScript</h6>
              <i className="fa fa-plus-square clicktoadd"></i>
              <i className="fa fa-check-square clicktoadd" style={{display:'none'}}></i>
            </div>
          </div>
          </div>
          
          <div className="mobileshow">
               <div className="col-lg-3 col-md-3 col-sm-6">
            <div className="options_box">
              <h6 className="m-0 mr-3">JavaScript</h6>
              <i className="fa fa-plus-square clicktoadd"></i>
              <i className="fa fa-check-square clicktoadd" style={{display:'none'}}></i>
            </div>
          </div>
          <div className="col-lg-3 col-md-3 col-sm-6">
            <div className="options_box">
              <h6 className="m-0 mr-3">JavaScript</h6>
              <i className="fa fa-plus-square clicktoadd"></i>
              <i className="fa fa-check-square clicktoadd" style={{display:'none'}}></i>
            </div>
          </div>
          </div>
       
      

         
        </div>
      </div>

      <div className="select_options">
        <div className="row justify-content-between">
          <div className="mobileshow">
              <div className="col-lg-3 col-md-3 col-sm-6">
            <div className="options_box">
              <h6 className="m-0 mr-3">JavaScript</h6>
              <i className="fa fa-plus-square clicktoadd"></i>
              <i className="fa fa-check-square clicktoadd" style={{display:'none'}}></i>
            </div>
          </div>
        <div className="col-lg-3 col-md-3 col-sm-6">
            <div className="options_box">
              <h6 className="m-0 mr-3">JavaScript</h6>
              <i className="fa fa-plus-square clicktoadd"></i>
              <i className="fa fa-check-square clicktoadd" style={{display:'none'}}></i>
            </div>
          </div>
          </div>
        
          <div className="mobileshow">
              <div className="col-lg-3 col-md-3 col-sm-6">
            <div className="options_box">
              <h6 className="m-0 mr-3">JavaScript</h6>
              <i className="fa fa-plus-square clicktoadd"></i>
              <i className="fa fa-check-square clicktoadd" style={{display:'none'}}></i>
            </div>
          </div>
          <div className="col-lg-3 col-md-3 col-sm-6">
            <div className="options_box">
              <h6 className="m-0 mr-3">JavaScript</h6>
              <i className="fa fa-plus-square clicktoadd"></i>
              <i className="fa fa-check-square clicktoadd" style={{display:'none'}}></i>
            </div>
          </div>
          </div>
        
      

         
        </div>
      </div>

      <div className="select_options">
        <div className="row justify-content-between">

          <div className="mobileshow">
                <div className="col-lg-3 col-md-3 col-sm-6">
            <div className="options_box">
              <h6 className="m-0 mr-3">JavaScript</h6>
              <i className="fa fa-plus-square clicktoadd"></i>
              <i className="fa fa-check-square clicktoadd" style={{display:'none'}}></i>
            </div>
          </div>
        <div className="col-lg-3 col-md-3 col-sm-6">
            <div className="options_box">
              <h6 className="m-0 mr-3">JavaScript</h6>
              <i className="fa fa-plus-square clicktoadd"></i>
              <i className="fa fa-check-square clicktoadd" style={{display:'none'}}></i>
            </div>
          </div>
          </div>
      
          <div className="mobileshow">
             <div className="col-lg-3 col-md-3 col-sm-6">
            <div className="options_box">
              <h6 className="m-0 mr-3">JavaScript</h6>
              <i className="fa fa-plus-square clicktoadd"></i>
              <i className="fa fa-check-square clicktoadd" style={{display:'none'}}></i>
            </div>
          </div>
          <div className="col-lg-3 col-md-3 col-sm-6">
            <div className="options_box">
              <h6 className="m-0 mr-3">JavaScript</h6>
              <i className="fa fa-plus-square clicktoadd"></i>
              <i className="fa fa-check-square clicktoadd" style={{display:'none'}}></i>
            </div>
          </div>
          </div>
         
      

         
        </div>
      </div>

      <div className="select_options">
        <div className="row justify-content-between">

          <div className="mobileshow">
              <div className="col-lg-3 col-md-3 col-sm-6">
            <div className="options_box">
              <h6 className="m-0 mr-3">JavaScript</h6>
              <i className="fa fa-plus-square clicktoadd"></i>
              <i className="fa fa-check-square clicktoadd" style={{display:'none'}}></i>
            </div>
          </div>
        <div className="col-lg-3 col-md-3 col-sm-6">
            <div className="options_box">
              <h6 className="m-0 mr-3">JavaScript</h6>
              <i className="fa fa-plus-square clicktoadd"></i>
              <i className="fa fa-check-square clicktoadd" style={{display:'none'}}></i>
            </div>
          </div>
          </div>
        
          <div className="mobileshow">
               <div className="col-lg-3 col-md-3 col-sm-6">
            <div className="options_box">
              <h6 className="m-0 mr-3">JavaScript</h6>
              <i className="fa fa-plus-square clicktoadd"></i>
              <i className="fa fa-check-square clicktoadd" style={{display:'none'}}></i>
            </div>
          </div>
          <div className="col-lg-3 col-md-3 col-sm-6">
            <div className="options_box">
              <h6 className="m-0 mr-3">JavaScript</h6>
              <i className="fa fa-plus-square clicktoadd"></i>
              <i className="fa fa-check-square clicktoadd" style={{display:'none'}}></i>
            </div>
          </div>
          </div>
       
      

         
        </div>
      </div>

      <div className="select_options">
        <div className="row justify-content-between">

          <div className="mobileshow">
              <div className="col-lg-3 col-md-3 col-sm-6">
            <div className="options_box">
              <h6 className="m-0 mr-3">JavaScript</h6>
              <i className="fa fa-plus-square clicktoadd"></i>
              <i className="fa fa-check-square clicktoadd" style={{display:'none'}}></i>
            </div>
          </div>
        <div className="col-lg-3 col-md-3 col-sm-6">
            <div className="options_box">
              <h6 className="m-0 mr-3">JavaScript</h6>
              <i className="fa fa-plus-square clicktoadd"></i>
              <i className="fa fa-check-square clicktoadd" style={{display:'none'}}></i>
            </div>
          </div>
          </div>
        
          <div className="mobileshow">
              <div className="col-lg-3 col-md-3 col-sm-6">
            <div className="options_box">
              <h6 className="m-0 mr-3">JavaScript</h6>
              <i className="fa fa-plus-square clicktoadd"></i>
              <i className="fa fa-check-square clicktoadd" style={{display:'none'}}></i>
            </div>
          </div>
          <div className="col-lg-3 col-md-3 col-sm-6">
            <div className="options_box">
              <h6 className="m-0 mr-3">JavaScript</h6>
              <i className="fa fa-plus-square clicktoadd"></i>
              <i className="fa fa-check-square clicktoadd" style={{display:'none'}}></i>
            </div>
          </div>
          </div>
        
      

         
        </div>
      </div>

      <div className="select_options mb-4">
        <div className="row justify-content-between">

          <div className="mobileshow">
               <div className="col-lg-3 col-md-3 col-sm-6">
            <div className="options_box">
              <h6 className="m-0 mr-3">JavaScript</h6>
              <i className="fa fa-plus-square clicktoadd"></i>
              <i className="fa fa-check-square clicktoadd" style={{display:'none'}}></i>
            </div>
          </div>
        <div className="col-lg-3 col-md-3 col-sm-6">
            <div className="options_box">
              <h6 className="m-0 mr-3">JavaScript</h6>
              <i className="fa fa-plus-square clicktoadd"></i>
              <i className="fa fa-check-square clicktoadd" style={{display:'none'}}></i>
            </div>
          </div>
          </div>
       
          <div className="mobileshow">
             <div className="col-lg-3 col-md-3 col-sm-6">
            <div className="options_box">
              <h6 className="m-0 mr-3">JavaScript</h6>
              <i className="fa fa-plus-square clicktoadd"></i>
              <i className="fa fa-check-square clicktoadd" style={{display:'none'}}></i>
            </div>
          </div>
          <div className="col-lg-3 col-md-3 col-sm-6">
            <div className="options_box">
              <h6 className="m-0 mr-3">JavaScript</h6>
              <i className="fa fa-plus-square clicktoadd"></i>
              <i className="fa fa-check-square clicktoadd" style={{display:'none'}}></i>
            </div>
          </div>
          </div>
         
      

         
        </div>
      </div>

      <select className="form-control">
        <option value="1">#2 Movies</option>
        <option value="2">#2 Movies</option>
        <option value="3">#3 Movies</option>
      </select>
    </section>

    <section className="thirdsection">
      <div className="mt-5 mb-5">
        <img src="img/SeekerIn.png" alt="logo" width="25%" />
      </div>

      <div className="animation_loader">
        <img src="img/Icon.png" alt="logo" width="15%" />
         <div className="aylana"></div>
      </div>

     

      <p>
        Please Wait <br />
        While We Setup for You
      </p>
    </section>

    <div className="button" id="next">Next</div>

 </div>
  );
}

export default App;
